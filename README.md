# custom drone build

custom drone build for alpine linux, i686
- with notes on arm64

## get src & deps

```
$ go get -u github.com/drone/drone
$ go get -u github.com/drone/drone-ui/dist
$ go get -u golang.org/x/net/context
$ go get -u golang.org/x/net/context/ctxhttp
$ go get -u github.com/golang/protobuf/proto
$ go get -u github.com/golang/protobuf/protoc-gen-go
$ cd ~/go/src/github.com/drone/drone
```

## build

```
$ GOOS=linux GOARCH=386 go build -ldflags -X github.com/drone/drone/version.VersionDev=build.tsuki -o release/drone-agent github.com/drone/drone/cmd/drone-agent
$ GOOS=linux GOARCH=386 go build -ldflags -X github.com/drone/drone/version.VersionDev=build.tsuki -o release/drone-server github.com/drone/drone/cmd/drone-server
```

```
$ docker build -t drone/drone:tsuki -f Dockerfile.alpine .
$ docker build -t drone/agent:tsuki -f Dockerfile.agent.alpine .
```

* add `CGO_ENABLED=0` (maybe?)
  - had some issues with disabling runtime/cgo, leaving it out helped
* alpine repo to match system
  - `... FROM alpine:edge`
* expose ports
  - change to `.. 8000 8080 9000`
* `Dockerfile.alpine` is missing a dependency 
  - add `RUN apk add -U --no-cache musl-dev` 

## arm build notes

* alter the go build
  - `GOARCH=arm` or `GOARCH=arm64`
* use arm specific drone-agent docker file
  - `Dockerfile.agent.linux.arm` or `Dockerfile.agent.linux.arm64`
* use root docker file for drone-server
  - maybe?

## docker-compose file

[docker-compose.yml](docker-compose.yml)

## drone-server docker build file

[Dockerfile.alpine](Dockerfile.alpine)

## drone-agent docker build file

[Dockerfile.agent.alpine](Dockerfile.agent.alpine)

## nginx config

[nginx.conf](drone.conf)

## drone openrc init file

[drone](drone)
